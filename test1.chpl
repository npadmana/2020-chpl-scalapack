// Let us define the problem we want to solve, and then we can
// figure out how we're actually going to solve it.

// Problem  (assume zero-indexed)
// C = A*B + 2 C
// Initially,
//   A = (5 x 7) matrix = i+2j
//   B = (7 x 3) matrix = 2i+j
//   C = (5 x 3) matrix = i+j
//
// See the attached Mathematica notebook for how this works.
//
// Now, an interesting complication here is that ScaLAPACK assumes
// column major arrays, so we want to do everything with transposed data.
// Unfortunately, that also involves setting up the processor grid in the same
// way.
//
//
// TODO - ScaLAPACKDist should have column-major data


use ScaLAPACK;

// Array dimensions
const M = 5,
  N=7,
  K=3;

// Block sizes
config const mBlock=2;
config const nBlock=2;
const blocksize=(mBlock, nBlock);

// Processor grid
config const npRow=2,
  npCol=3;
assert(numLocales == (npRow*npCol),"Processor grid not the right shape");

writeln("Hello, ScaLAPACK! This is Chapel calling!");

// Set up my initial BLACS context.
// PBLAS routines must use the same context
var myContext = setBLACSContext((npRow, npCol));

// Now define the domains and arrays
// Also print much debugging information
//A
var (Adom,Adesc) = newSLDom((M,N), blocksize, myContext);
var A : [Adom] real;
forall (i,j) in Adom do A[i,j] = (i + 2*j):real;
printInfo(A, Adom, Adesc);

//B
var (Bdom,Bdesc) = newSLDom((N,K), blocksize, myContext);
var B : [Bdom] real;
forall (i,j) in Bdom do B[i,j] = (2*i + j):real;
printInfo(B, Bdom, Bdesc);

//C
var (Cdom,Cdesc) = newSLDom((M,K), blocksize, myContext);
var C : [Cdom] real;
forall (i,j) in Cdom do C[i,j] = (i + j):real;
printInfo(C, Cdom, Cdesc);


// Do the matrix multiply
coforall loc in Locales {
  on loc {
    var tstr = "N".c_str();
    var myM = M : c_int;
    var myN = N : c_int;
    var myK = K : c_int;
    var aptr = A.localArrayPtr() : c_ptr(c_double);
    var bptr = B.localArrayPtr() : c_ptr(c_double);
    var cptr = C.localArrayPtr() : c_ptr(c_double);
    var desca = c_ptrTo(Adesc[here.id].Adesc[0]);
    var descb = c_ptrTo(Bdesc[here.id].Adesc[0]);
    var descc = c_ptrTo(Cdesc[here.id].Adesc[0]);
    var ia = 1 : c_int; // use the full matrix (ugh, 1 indexed)
    var ja = 1 : c_int;
    var alpha = 1.0 : c_double;
    var beta = 2.0 : c_double;
    // Ooops, my definition of M, N, K don't quite correspond
    // to ScaLAPACK
    pdgemm("N".c_str(), "N".c_str(), myK, myM, myN, 
           alpha,
           bptr, ia, ja, descb,
           aptr, ia, ja, desca,
           beta,
           cptr, ia, ja, descc);
  }
}

writeln("============= C ===============");
writeln(C);
writeln("===============================");

var Ctrue = reshape([364, 408, 452,
                     408, 459, 510,
                     452, 510, 568,
                     496, 561, 626,
                     540, 612, 684], {0..<M,0..<K});

var diff = + reduce(abs(C - Ctrue));
if (diff==0) {
  writeln("Validated! Success! Chapel has called ScaLAPACK!!!!");
} else {
  writeln("Failed! :-(");
  writeln(Ctrue);
}

