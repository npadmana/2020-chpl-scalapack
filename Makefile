SCALAPACK=-lmkl_scalapack_lp64 -lmkl_blacs_intelmpi_lp64

test1: test1.chpl ScaLAPACK.chpl
	chpl -o $@ $< ${MKL_CHPL_LINK} ${MPI_CHPL_FLAGS} ${SCALAPACK}
