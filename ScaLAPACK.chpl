module ScaLAPACK {

  use MPI;
  use PrivateDist;
  public use SysCTypes;
  public use BlockCycDist; // This is currently just BlockCycDist -- I made a copy to hack at it.
  require "mkl_blacs.h";
  require "mkl_pblas.h";
  require "mkl_scalapack.h";

  proc deinit() {
    // Shutdown -- don't close out MPI
    // In general, this could largely move over to the module code.
    coforall loc in Locales do on loc {
        var inot : c_int = 1;
        blacs_exit(inot);
    }
  }



  // Process number to locale mapping
  // Does not have to be the same
  config const debugBLACSid=true;
  if debugBLACSid {
    for loc in Locales do on loc {
        var mypnum : c_int;
        var nprocs : c_int;
        blacs_pinfo(mypnum, nprocs);
        writeln("Locale ", here.id," of ",numLocales,": BLACS process id ",mypnum," of ",nprocs);
      }
    writeln();
  }


  record SLContext {
    var npRow, npCol : int;
    var ctxt : [PrivateSpace] c_int;
  }


  proc setBLACSContext(nProcs : 2*int) {
    var ret : SLContext;
    const (npRow, npCol) = nProcs;
    ret.npRow = npRow;
    ret.npCol = npCol;
    ref context = ret.ctxt;

    // Set up a context
    coforall loc in Locales do on loc {
        var ictxt : c_int = -1;
        var iwhat : c_int = 0;
        var ilay : c_char = 67 : c_char; // "C"
        var mynrow = npRow : c_int;
        var myncol = npCol : c_int;
        var myprow, mypcol : c_int;

        // Get a context
        ref myctxt = context[here.id];
        blacs_get(ictxt, iwhat, myctxt);
        blacs_gridinit(myctxt, ilay, myncol, mynrow); //Transposed

        // Validate and set targetLocales
        blacs_gridinfo(myctxt, myncol, mynrow, mypcol, myprow); //Transposed
        if (myncol != npCol) {
          writeln("Column mismatched! ",myncol,"!=",npCol);
          exit(1);
        }
        if (mynrow != npRow) {
          writeln("Row mismatched! ",mynrow,"!=",npRow);
          exit(1);
        }
      }

    // Can return any context
    return ret;
  }


  record MatrixDescriptor {
    var context : c_int;
    var local_nrow, local_ncol : c_int;
    var lld : c_int;
    var Adesc : c_array(c_int,9);
  }




  proc newSLDom(dims, blocksize, context) {
    const (M,N) = dims;
    const (npRow, npCol) = (context.npRow, context.npCol);
    const Space = {0..<M,0..<N};
    var desc : [PrivateSpace] MatrixDescriptor;
    var targetLocales : [{0..<npRow,0..<npCol}] locale;

    // Set up a context
    coforall loc in Locales do on loc {
        ref mydesc = desc[here.id];
        mydesc.context = context.ctxt[here.id];
        ref myctxt = mydesc.context;
    
        var mynrow, myncol, myprow, mypcol : c_int;
        // Validate and set targetLocales
        blacs_gridinfo(myctxt, myncol, mynrow, mypcol, myprow); //Transposed
        targetLocales[myprow, mypcol] = here; // Save the locale

        // Get the number of local rows and columns
        var irsrc = 0 : c_int;
        var icsrc = 0 : c_int;
        var Nr = M : c_int;
        var Nc = N : c_int;
        var (mymb, mynb) = (blocksize[0]:c_int,blocksize[1]:c_int);
        mydesc.local_ncol = numroc(Nc, mynb, mypcol, icsrc, myncol);
        mydesc.local_nrow = numroc(Nr, mymb, myprow, irsrc, mynrow);
        var lld1 = ((mydesc.local_ncol + mynb-1)/mynb):c_int * mynb;
        mydesc.lld = if lld1==0 then 1:c_int else lld1;
        var info : c_int;
        descinit(c_ptrTo(mydesc.Adesc[0]) , Nc, Nr, mynb, mymb, icsrc, irsrc, myctxt, mydesc.lld, info);
        if (info != 0) {
          writeln("Failed to get array descriptor! Incorrect option", -info);
          exit(1);
        }
      }

    var dom : domain(2)
      dmapped BlockCyclic(Space.low, blocksize=blocksize, targetLocales=targetLocales)
      = Space;

    return (dom, desc);

  }



  // Debugging
  proc printInfo(A, Adom, Adesc) {
    writeln();
    writeln("======= Layout ==============="); printLayout(Adom);
    writeln("====== Data ==================");
    writeln(A);
    writeln("------------------------------");
    for loc in Locales do on loc {
        writeln("Locale ",here.id," : ",Adesc[here.id]);
        writeln("Local buffer :",A.localArray());
      }
    writeln("==============================");
    writeln();
    
    proc printLayout(dom) {
      var arr : [dom] int;
      forall a in arr do a=a.locale.id;
      writeln(arr);
    }
  }



  // Hack BlockCyclicArr to return some useful bits of information
  proc _array.localArray() ref {
    return _value.dsilocalArray();
  }

  proc BlockCyclicArr.dsilocalArray() ref {
    return myLocArr!.myElems;
  }

  proc _array.localArrayPtr() {
    return _value.dsilocalArrayPtr();
  }

  proc BlockCyclicArr.dsilocalArrayPtr() {
    ref arr = myLocArr!.myElems;
    if (arr.size > 0) {
      return c_ptrTo(myLocArr!.myElems);
    } else {
      return c_nil;
    }
  }



  // ASSUMES MKL_INT is int. This should be checked.
  extern proc descinit(desc : c_ptr(c_int), ref m : c_int, ref n : c_int,
                       ref mb : c_int, ref nb : c_int,
                       ref irsrc : c_int, ref icsrc : c_int,
                       ref ictxt : c_int, ref lld : c_int,
                       ref info : c_int);
  extern proc numroc(ref n : c_int, ref nb : c_int, ref iproc : c_int,
                     ref isrcproc : c_int, ref nprocs : c_int) : c_int;


  // pdgemm
  extern proc pdgemm(transa : c_string, transb : c_string,
                     ref m : c_int, ref n : c_int, ref k : c_int,
                     ref alpha : c_double,
                     a : c_ptr(c_double), ref ia : c_int, ref ja : c_int, desca : c_ptr(c_int),
                     b : c_ptr(c_double), ref ib : c_int, ref jb : c_int, descb : c_ptr(c_int),
                     ref beta : c_double,                                 
                     c : c_ptr(c_double), ref ic : c_int, ref jc : c_int, descc : c_ptr(c_int));


  // Basic BLACS routines
  extern proc blacs_get(ref ConTxt : c_int, ref what : c_int, ref val : c_int);
  extern proc blacs_exit(ref notDone : c_int);
  extern proc blacs_gridinit(ref icontxt : c_int, ref layout : c_char, ref nprow : c_int, ref npcol : c_int);
  extern proc blacs_gridinfo(ref icontxt : c_int, ref nprow : c_int, ref npcol : c_int, ref myprow : c_int, ref mypcol : c_int);
  extern proc blacs_pinfo(ref mypnum : c_int, ref nprocs : c_int);

}